# OpenML dataset: Lung

https://www.openml.org/d/45093

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Lung dataset, 203 instances and 5 classes**

**Authors**: A. Bhattacharjee, W. Richards, J. Staunton, C. Li, S. Monti, P. Vasa, C. Ladd, J. Beheshti, R. Bueno, M. Gillette, et al

**Please cite**: ([URL](https://www.pnas.org/doi/abs/10.1073/pnas.191502998)): A. Bhattacharjee, W. Richards, J. Staunton, C. Li, S. Monti, P. Vasa, C. Ladd, J. Beheshti, R. Bueno, M. Gillette, et al, Classification of human lung carcinomas by mrna expression profiling reveals distinct adenocarcinoma subclasses, Proc. Nat. Acad. Sci. 98 (24) (2001) 13790-13795.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45093) of an [OpenML dataset](https://www.openml.org/d/45093). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45093/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45093/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45093/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

